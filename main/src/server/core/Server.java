package server.core;

import org.jetbrains.annotations.NotNull;
import server.core.api.ApiServer;

/**
 * Created by Inventor
 */
final class Server {
    @NotNull
    private final ApiServer apiServer;

    public static void main(String[] args) {
        try {
            new Server();
        } catch (Exception e) {
            System.exit(2);
        }
    }

    private Server() throws Exception {
        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));

        apiServer = new ApiServer("ApiServer", 8010);
        apiServer.getServer().start();
    }

    private void shutdown() {
        try {
            apiServer.shutdown();
        } catch (Exception ignore) {
        }
    }
}
