package server.core.api;

import com.sun.net.httpserver.HttpServer;
import org.jetbrains.annotations.NotNull;
import server.api.ThreadFactoryImpl;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Inventor
 */
public final class ApiServer {
    @NotNull
    private final String name;
    @NotNull
    private final HttpServer server;
    @NotNull
    private final ExecutorService pool;

    public ApiServer(@NotNull String name, int port) throws IOException {
        this.name = name;
        this.server = HttpServer.create(new InetSocketAddress(port), 0);
        this.server.setExecutor(pool = new ThreadPoolExecutor(10, Integer.MAX_VALUE, 20L, TimeUnit.MINUTES, new SynchronousQueue<>(), new ThreadFactoryImpl(name)));
        createContext();
    }

    private void createContext() {
        server.createContext("/login", new HttpHandlerLogin());
        server.createContext("/logout", new HttpHandlerLogout());
        server.createContext("/payment", new HttpHandlerPayment());
    }

    @NotNull
    public String getName() {
        return name;
    }

    @NotNull
    public HttpServer getServer() {
        return server;
    }

    public void shutdown() {
        server.stop(1);
        pool.shutdown();
    }
}
