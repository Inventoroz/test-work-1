package server.core.api;

import com.google.common.hash.Hashing;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import server.api.AHttpHandler;
import server.api.EError;
import server.api.EHeader;
import server.api.NetworkException;
import server.core.DB;
import server.db.entities.AccountDB;

import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Objects;
import java.util.StringTokenizer;

/**
 * Created by Inventor
 */
class HttpHandlerAccount extends AHttpHandler {
    @NotNull
    final AccountDB getAccountDB(@NotNull Map<EHeader, String> requestHeaderMap) throws NetworkException {
        String login = null;
        String password = null;
        String value = requestHeaderMap.get(EHeader.AUTHORIZATION);
        if (value != null && value.length() > 5) {
            String authorization = new String(DatatypeConverter.parseBase64Binary(value.substring(6)), StandardCharsets.UTF_8);
            StringTokenizer st = new StringTokenizer(authorization, ":");
            login = st.hasMoreTokens() ? st.nextToken() : null;
            password = st.hasMoreTokens() ? st.nextToken() : null;
        }
        if (login == null || login.isEmpty()) throw new NetworkException(EError.LOGIN_NOT);
        if (password == null || password.isEmpty()) throw new NetworkException(EError.PASSWORD_NOT);

        AccountDB accountDB = DB.getAccountService().getAccountByLogin(login);
        if (accountDB == null) throw new NetworkException(EError.ACCOUNT_NOT_FOUND);
        if (!Objects.equals(accountDB.getPassword(), createPasswordHash(password))) throw new NetworkException(EError.PASSWORD_NOT_CORRECT);
        return accountDB;
    }

    @Nullable
    private static String createPasswordHash(@Nullable String password) {
        if (password == null) return null;
        return md5(md5(password) + md5("salt"));
    }

    @NotNull
    private static String md5(@NotNull String text) {
        return Hashing.md5().hashString(text, StandardCharsets.UTF_8).toString().toUpperCase();
    }
}
