package server.core.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import server.api.EHeader;
import server.api.NetworkException;
import server.core.DB;
import server.db.entities.TokenDB;

import java.util.Map;

/**
 * Created by Inventor
 */
public final class HttpHandlerLogout extends HttpHandlerToken {
    @Nullable
    @Override
    protected Object doGet(@NotNull Map<String, String> parameterMap, @NotNull Map<EHeader, String> requestHeaderMap) throws NetworkException {
        TokenDB tokenDB = getTokenDB(parameterMap, requestHeaderMap);
        DB.getTokenService().delete(tokenDB.getObjectId());
        return null;
    }
}
