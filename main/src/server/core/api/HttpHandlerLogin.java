package server.core.api;

import org.jetbrains.annotations.NotNull;
import server.api.EError;
import server.api.EHeader;
import server.api.NetworkException;
import server.core.DB;
import server.db.entities.AccountDB;
import server.db.entities.TokenDB;

import java.time.ZonedDateTime;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Inventor
 */
public final class HttpHandlerLogin extends HttpHandlerAccount {
    @NotNull
    @Override
    protected String doGet(@NotNull Map<String, String> parameterMap, @NotNull Map<EHeader, String> requestHeaderMap) throws NetworkException {
        AccountDB accountDB = getAccountDB(requestHeaderMap);

        ZonedDateTime now = ZonedDateTime.now();
        ZonedDateTime expiration = now.plusMinutes(5);//Время действия токена 5 минут
        TokenDB tokenDB = DB.getTokenService().save(new TokenDB(accountDB.getObjectId(), UUID.randomUUID().toString(), now.toInstant().toEpochMilli(), expiration.toInstant().toEpochMilli()));
        if (tokenDB == null) throw new NetworkException(EError.AUTHORIZATION_NOT);

        return tokenDB.getToken();
    }
}
