package server.core.api;

import org.jetbrains.annotations.NotNull;
import server.api.EError;
import server.api.EHeader;
import server.api.NetworkException;
import server.core.DB;
import server.db.entities.AccountDB;
import server.db.entities.TokenDB;

import java.util.Map;

/**
 * Created by Inventor
 */
class HttpHandlerToken extends HttpHandlerAccount {
    @NotNull
    final TokenDB getTokenDB(@NotNull Map<String, String> parameterMap, @NotNull Map<EHeader, String> requestHeaderMap) throws NetworkException {
        String token = parameterMap.get("token");
        if (token == null || token.isEmpty()) throw new NetworkException(EError.TOKEN_NOT);

        TokenDB tokenDB = DB.getTokenService().getTokenByToken(token);
        if (tokenDB == null) throw new NetworkException(EError.TOKEN_NOT_FOUND);

        AccountDB accountDB = getAccountDB(requestHeaderMap);
        if (tokenDB.getAccountId() != accountDB.getObjectId()) throw new NetworkException(EError.TOKEN_OTHER_ACCOUNT);
        return tokenDB;
    }
}
