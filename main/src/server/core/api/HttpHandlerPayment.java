package server.core.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import server.api.EError;
import server.api.EHeader;
import server.api.NetworkException;
import server.core.DB;
import server.db.entities.AccountDB;
import server.db.entities.BalanceDB;
import server.db.entities.TokenDB;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Map;

/**
 * Created by Inventor
 */
public final class HttpHandlerPayment extends HttpHandlerToken {
    @Nullable
    @Override
    protected Object doPost(@NotNull Map<String, String> parameterMap, @NotNull Map<EHeader, String> requestHeaderMap) throws NetworkException {
        TokenDB tokenDB = getTokenDB(parameterMap, requestHeaderMap);

        ZonedDateTime now = ZonedDateTime.now();
        ZonedDateTime expiration = ZonedDateTime.ofInstant(Instant.ofEpochMilli(tokenDB.getExpiration()), ZoneId.systemDefault());
        if (now.isAfter(expiration)) throw new NetworkException(EError.TOKEN_EXPIRATION);

        AccountDB accountDB = getAccountDB(requestHeaderMap);
        BalanceDB balanceDB = DB.getBalanceService().get(accountDB.getObjectId());
        if (balanceDB == null) throw new NetworkException(EError.BALANCE_NOT_FOUND);

        BigDecimal amount = new BigDecimal("-1.1");
        BigDecimal newBalance = balanceDB.getBalance().add(amount);
        if (newBalance.compareTo(BigDecimal.ZERO) < 0) throw new NetworkException(EError.BALANCE_NOT_FUNDS);

        if (!DB.getBalanceService().changeBalance(accountDB.getObjectId(), amount)) throw new NetworkException(EError.BALANCE_NOT_CHANGE);
        return null;
    }
}
