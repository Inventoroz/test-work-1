package server.core;

import org.jetbrains.annotations.NotNull;
import server.db.services.AccountService;
import server.db.services.BalanceService;
import server.db.services.TokenService;

/**
 * Created by Inventor
 */
public final class DB {
    @NotNull
    private static final AccountService ACCOUNT_SERVICE = new AccountService();
    @NotNull
    private static final BalanceService BALANCE_SERVICE = new BalanceService();
    @NotNull
    private static final TokenService TOKEN_SERVICE = new TokenService();

    @NotNull
    public static AccountService getAccountService() {
        return ACCOUNT_SERVICE;
    }

    @NotNull
    public static BalanceService getBalanceService() {
        return BALANCE_SERVICE;
    }

    @NotNull
    public static TokenService getTokenService() {
        return TOKEN_SERVICE;
    }
}
