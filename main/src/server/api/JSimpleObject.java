package server.api;

import org.jetbrains.annotations.Nullable;

/**
 * Created by Inventor
 */
final class JSimpleObject extends JSimpleMessage {
    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    @Nullable
    private Object data;

    JSimpleObject(@Nullable Object data) {
        super();
        this.data = data;
    }
}
