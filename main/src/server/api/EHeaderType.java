package server.api;

/**
 * Created by Inventor
 */
enum EHeaderType {
    REQUEST,//Залоговок используется только в запросе
    RESPONSE,//Залоговок используется только в ответе
    ALL//Залоговок используется и в запросе и в ответе
}
