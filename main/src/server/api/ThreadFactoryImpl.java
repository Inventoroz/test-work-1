package server.api;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * Created by Inventor
 */
public final class ThreadFactoryImpl implements ThreadFactory {
    @NotNull
    private final ThreadFactory defaultFactory = Executors.defaultThreadFactory();
    @NotNull
    private final String name;

    public ThreadFactoryImpl(@NotNull String name) {
        this.name = name;
    }

    @Override
    public Thread newThread(@NotNull Runnable r) {
        Thread thread = defaultFactory.newThread(r);
        thread.setName(name + thread.getName());
        return thread;
    }
}
