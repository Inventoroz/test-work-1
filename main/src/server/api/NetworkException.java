package server.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by Inventor
 */
public final class NetworkException extends Exception {
    private int code;

    public NetworkException(@NotNull EError error, Object... objects) {
        this(null, error, objects);
    }

    public NetworkException(@Nullable Throwable cause, @NotNull EError error, Object... objects) {
        super(error.getMessage(objects), cause);
        this.code = error.getCode();
    }

    int getCode() {
        return code;
    }
}
