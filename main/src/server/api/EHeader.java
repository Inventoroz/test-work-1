package server.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Inventor
 * https://ru.wikipedia.org/wiki/Список_заголовков_HTTP
 * https://developer.mozilla.org/ru/docs/Web/HTTP/Заголовки
 */
public enum EHeader {
    ACCEPT("Accept", EHeaderType.REQUEST),//Список допустимых форматов ресурса. Accept: text/plain
    ACCEPT_CHARSET("Accept-Charset", EHeaderType.REQUEST),//Перечень поддерживаемых кодировок для предоставления пользователю. Accept-Charset: utf-8
    ACCEPT_ENCODING("Accept-Encoding", EHeaderType.REQUEST),//Перечень поддерживаемых способов кодирования содержимого сущности при передаче. Accept-Encoding: <compress | gzip | deflate | sdch | identity>
    ACCEPT_LANGUAGE("Accept-Language", EHeaderType.REQUEST),//Список поддерживаемых естественных языков. Accept-Language: ru
    ACCEPT_RANGES("Accept-Ranges", EHeaderType.RESPONSE),//Перечень единиц измерения диапазонов. Accept-Ranges: bytes
    ACCESS_CONTROL_ALLOW_ORIGIN("Access-Control-Allow-Origin", EHeaderType.RESPONSE),//CORS
    ACCESS_CONTROL_ALLOW_METHODS("Access-Control-Allow-Methods", EHeaderType.RESPONSE),//CORS
    ACCESS_CONTROL_ALLOW_HEADERS("Access-Control-Allow-Headers", EHeaderType.RESPONSE),//CORS
    ACCESS_CONTROL_REQUEST_METHOD("Access-Control-Request-Method", EHeaderType.REQUEST),//CORS
    ACCESS_CONTROL_REQUEST_HEADERS("Access-Control-Request-Headers", EHeaderType.REQUEST),//CORS
    AGE("Age", EHeaderType.RESPONSE),//Количество секунд с момента модификации ресурса.
    ALLOW("Allow", EHeaderType.RESPONSE),//Список поддерживаемых методов. Allow: OPTIONS, GET, HEAD
    ALTERNATES("Alternates", EHeaderType.RESPONSE),//Указание на альтернативные способы представления ресурса.
    AUTHORIZATION("Authorization", EHeaderType.REQUEST),//Данные для авторизации. Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==
    CACHE_CONTROL("Cache-Control", EHeaderType.ALL),//Основные директивы для управления кэшированием. Cache-Control: no-store
    CONNECTION("Connection", EHeaderType.ALL),//Сведения о проведении соединения. Connection: close
    CONTENT_DISPOSITION("Content-Disposition", EHeaderType.ALL),//Способ распределения сущностей в сообщении при передаче нескольких фрагментов. Content-Disposition: form-data; name="AttachedFile1"; filename="photo-1.jpg"
    CONTENT_ENCODING("Content-Encoding", EHeaderType.ALL),//Способ кодирования содержимого сущности при передаче.
    CONTENT_LANGUAGE("Content-Language", EHeaderType.ALL),//Один или несколько естественных языков содержимого сущности. Content-Language: en, ase, ru
    CONTENT_LENGTH("Content-Length", EHeaderType.ALL),//Размер содержимого сущности в октетах (которые в русском языке обычно называют байтами). Content-Length: 1348
    CONTENT_LOCATION("Content-Location", EHeaderType.ALL),//Альтернативное расположение содержимого сущности.
    CONTENT_MD5("Content-MD5", EHeaderType.ALL),//Base64 MD5-хэша сущности для проверки целостности. Content-MD5: Q2hlY2sgSW50ZWdyaXR5IQ==
    CONTENT_RANGE("Content-Range", EHeaderType.ALL),//Байтовые диапазоны передаваемой сущности если возвращается фрагмент. Подробности: Частичные GET. Content-Range: bytes 88080384-160993791/160993792
    //https://ru.wikipedia.org/wiki/Список_MIME-типов
    CONTENT_TYPE("Content-Type", EHeaderType.ALL),//Формат и способ представления сущности. Content-Type: text/html;charset=utf-8
    CONTENT_VERSION("Content-Version", EHeaderType.ALL),//Информация о текущей версии сущности.
    COOKIE("Cookie", EHeaderType.REQUEST),//
    DATE("Date", EHeaderType.ALL),//Дата генерации отклика. Date: Tue, 15 Nov 1994 08:12:31 GMT
    DERIVED_FORM("Derived-From", EHeaderType.ALL),//Информация о текущей версии сущности.
    ETAG("ETag", EHeaderType.RESPONSE),//Тег (уникальный идентификатор) версии сущности, используемый при кэшировании. ETag: "56d-9989200-1132c580"
    EXPECT("Expect", EHeaderType.REQUEST),//Указывает серверу что клиент ожидает от него дополнительного действия. Expect: 100-continue
    EXPIRES("Expires", EHeaderType.ALL),//Дата предполагаемого истечения срока актуальности сущности. Expires: Tue, 31 Jan 2012 15:02:53 GMT
    FROM("From", EHeaderType.REQUEST),//Адрес электронной почты ответственного лица со стороны клиента. From: user@example.com
    HOST("Host", EHeaderType.REQUEST),//Доменное имя и порт хоста запрашиваемого ресурса. Необходимо для поддержки виртуального хостинга на серверах. Host: ru.wikipedia.org
    IF_MATCH("If-Match", EHeaderType.REQUEST),//Список тегов версий сущности. Выполнять метод, если они существуют. If-Match: "737060cd8c284d8af7ad3082f209582d"
    IF_MODIFIED_SINCE("If-Modified-Since", EHeaderType.REQUEST),//Дата. Выполнять метод если сущность изменилась с указанного момента. If-Modified-Since: Sat, 29 Oct 1994 19:43:31 GMT
    IF_NONE_MATCH("If-None-Match", EHeaderType.REQUEST),//Список тегов версий сущности. Выполнять метод если ни одного из них не существует. If-None-Match: "737060cd8c284d8af7ad3082f209582d"
    IF_RANGE("If-Range", EHeaderType.REQUEST),//Список тегов версий сущности или дата для определённого фрагмента сущности. If-Range: "737060cd8c284d8af7ad3082f209582d"
    IF_UNMODIFIED_SINCE("If-Unmodified-Since", EHeaderType.REQUEST),//Дата. Выполнять метод если сущность не изменилась с указанной даты. If-Unmodified-Since: Sat, 29 Oct 1994 19:43:31 GMT
    LAST_MODIFIED("Last-Modified", EHeaderType.ALL),//Дата последней модификации сущности.
    LINK("Link", EHeaderType.ALL),//Указывает на логически связанный с сущностью ресурс аналогично тегу <LINK> в HTML.
    LOCATION("Location", EHeaderType.RESPONSE),//URI по которому клиенту следует перейти или URI созданного ресурса. Location: http://example.com/about.html#contacts
    MAX_FORWARDS("Max-Forwards", EHeaderType.REQUEST),//Максимально допустимое количество переходов через прокси. Max-Forwards: 10
    MIME_VERSION("MIME-Version", EHeaderType.REQUEST),//Версия протокола MIME, по которому было сформировано сообщение.
    ORIGIN("Origin", EHeaderType.REQUEST),//Заголовок запроса показывает откуда будет производиться загрузка. Он не включает в себя какую-либо информацию о пути, содержит в себе лишь имя сервера. Заголовок отправляется как с CORS, так и с POST запросами. Он похож на заголовок Referer, но, в отличие от этого заголовка, не раскрывает весь путь. Origin: https://developer.mozilla.org
    PRAGMA("Pragma", EHeaderType.REQUEST),//Особенные опции выполнения операции. Pragma: no-cache
    PROXY_AUTHENTICATE("Proxy-Authenticate", EHeaderType.RESPONSE),//Параметры аутентификации на прокси-сервере.
    PROXY_AUTHORIZATION("Proxy-Authorization", EHeaderType.REQUEST),//Информация для авторизации на прокси-сервере. Proxy-Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==
    PUBLIC("Public", EHeaderType.RESPONSE),//Список доступных методов аналогично Allow, но для всего сервера.
    RANGE("Range", EHeaderType.REQUEST),//Байтовые диапазоны для запроса фрагментов ресурса. Подробности: Частичные GET. Range: bytes=50000-99999,250000-399999,500000-
    REFERER("Referer", EHeaderType.REQUEST),//URI ресурса, после которого клиент сделал текущий запрос. Referer: http://en.wikipedia.org/wiki/Main_Page
    RETRY_AFTER("Retry-After", EHeaderType.RESPONSE),//Дата или время в секундах после которого можно повторить запрос.
    SERVER("Server", EHeaderType.RESPONSE),//Список названий и версий веб-сервера и его компонентов с комментариями. Для прокси-серверов поле Via. Server: Apache/2.2.17 (Win32) PHP/5.3.5
    SET_COOKIE("Set-Cookie", EHeaderType.RESPONSE),//
    TITLE("Title", EHeaderType.ALL),//Заголовок сущности.
    TE("TE", EHeaderType.REQUEST),//Список расширенных способов кодирования при передаче. TE: trailers, deflate
    TRAILER("Trailer", EHeaderType.ALL),//Список полей, имеющих отношение к кодированию сообщения при передаче.
    TRANSFER_ENCODING("Transfer-Encoding", EHeaderType.ALL),//Список способов кодирования, которые были применены к сообщению для передачи. Transfer-Encoding: chunked
    UPGRADE("Upgrade", EHeaderType.ALL),//Список предлагаемых клиентом протоколов. Сервер указывает один протокол. Upgrade: HTTP/2.0, SHTTP/1.3, IRC/6.9, RTA/x11
    USER_AGENT("User-Agent", EHeaderType.REQUEST),//Список названий и версий клиента и его компонентов с комментариями. User-Agent: Mozilla/5.0 (X11; Linux i686; rv:2.0.1) Gecko/20100101 Firefox/4.0.1
    VARY("Vary", EHeaderType.RESPONSE),//Список описывающих ресурс полей из запроса, которые были приняты во внимание. Vary: Accept-Encoding
    VIA("Via", EHeaderType.ALL),//Список версий протокола, названий и версий прокси-серверов, через которых прошло сообщение. Via: 1.0 fred, 1.1 nowhere.com (Apache/1.1)
    WARNING("Warning", EHeaderType.ALL),//Код, агент, сообщение и дата, если возникла критическая ситуация. Warning: 199 Miscellaneous warning
    WWW_AUTHENTICATE("WWW-Authenticate", EHeaderType.RESPONSE),//Параметры аутентификации для выполнения метода к указанному ресурсу.
    ;

    @NotNull
    private final String name;
    @NotNull
    private final EHeaderType type;

    EHeader(@NotNull String name, @NotNull EHeaderType type) {
        this.name = name;
        this.type = type;
    }

    @NotNull
    String getName() {
        return name;
    }

    @NotNull
    EHeaderType getType() {
        return type;
    }

    @Nullable
    static EHeader getHeader(@Nullable String value) {
        if (value == null || value.isEmpty()) return null;
        for (EHeader header : values()) if (header.name.equalsIgnoreCase(value)) return header;
        return null;
    }

    @Nullable
    static Map<EHeader, String> createHeaderMap(@Nullable Map<String, List<String>> valueMap, @Nullable EHeaderType headerType) {
        if (valueMap == null || valueMap.isEmpty()) return null;
        Map<EHeader, String> headerMap = new EnumMap<>(EHeader.class);
        for (Map.Entry<String, List<String>> entry : valueMap.entrySet()) {
            EHeader header = getHeader(entry.getKey());
            if (header == null) continue;
            if (headerType != null) {
                if (headerType == EHeaderType.REQUEST && header.type == EHeaderType.RESPONSE) continue;
                if (headerType == EHeaderType.RESPONSE && header.type == EHeaderType.REQUEST) continue;
            }
            headerMap.put(header, entry.getValue().get(0));
        }
        return headerMap;
    }


    @Override
    public String toString() {
        return getName();
    }
}
