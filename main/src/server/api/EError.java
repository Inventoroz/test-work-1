package server.api;

import org.jetbrains.annotations.NotNull;

/**
 * Created by Inventor
 */
public enum EError {
    UNSUPPORTED_METHOD("Метод \"%s\" не поддерживается для данного запроса"),
    NETWORK_UNKNOWN_ERROR("Неизвестная ошибка в обработке запроса/ответа"),
    REQUEST_BODY_NOT_GET("Не удалось получить тело запроса"),
    CONTENT_TYPE_NOT("Отсутствует заголовок content-type"),
    CONTENT_TYPE_NOT_CORRECT("Неверное значение заголовка content-type"),
    REQUEST_HEADERS_NOT("Отсутствуют заголовки у запроса"),
    LOGIN_NOT("Отсутствует логин"),
    PASSWORD_NOT("Отсутствует пароль"),
    ACCOUNT_NOT_FOUND("Аккаунт не существует"),
    PASSWORD_NOT_CORRECT("Неверный пароль"),
    AUTHORIZATION_NOT("Не удалось авторизоваться"),
    TOKEN_NOT("Отсутствует токен"),
    TOKEN_NOT_FOUND("Токен не существует"),
    TOKEN_EXPIRATION("Токен не действителен"),
    TOKEN_OTHER_ACCOUNT("Чужой токен"),
    BALANCE_NOT_FOUND("Баланс не существует"),
    BALANCE_NOT_FUNDS("На балансе недостаточно средств"),
    BALANCE_NOT_CHANGE("Не удалось списать средства"),
    ;

    @NotNull
    private final String message;

    EError(@NotNull String message) {
        this.message = message;
    }

    @NotNull
    String getMessage(@NotNull Object... objects) {
        return objects.length == 0 ? message : String.format(message, objects);
    }

    int getCode() {
        return -1000 - ordinal();
    }
}
