package server.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by Inventor
 */
final class JsonHelper {
    @Nullable
    private static Gson gson;

    @SuppressWarnings("SpellCheckingInspection")
    @NotNull
    private static Gson getGson() {
        if (gson == null) gson = new GsonBuilder().serializeNulls().create();
        return gson;
    }

    @NotNull
    static <T> T fromJson(@NotNull String json, @NotNull Class<T> objectClass) throws JsonParseException {
        return getGson().fromJson(json, objectClass);
    }

    @NotNull
    static String toJson(@NotNull Object object) {
        return getGson().toJson(object);
    }
}
