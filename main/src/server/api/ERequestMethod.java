package server.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by Inventor
 */
enum ERequestMethod {
    GET,
    POST,
    PUT,
    DELETE,
    ;

    @NotNull
    public String getId() {
        return name();
    }

    @Nullable
    static ERequestMethod getRequestMethod(@Nullable String value) {
        if (value == null || value.isEmpty()) return null;
        for (ERequestMethod requestMethod : values()) if (requestMethod.getId().equals(value)) return requestMethod;
        return null;
    }

    @NotNull
    @Override
    public String toString() {
        return name();
    }
}
