package server.api;

import org.jetbrains.annotations.NotNull;

/**
 * Created by Inventor
 */
class JSimpleMessage extends JSimple {
    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    private int code;
    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    @NotNull
    private String message = "";

    JSimpleMessage() {
        super(true);
    }

    JSimpleMessage(@NotNull EError error, @NotNull Object... objects) {
        this(error.getCode(), error.getMessage(objects));
    }

    JSimpleMessage(int code, @NotNull String message) {
        super(false);
        this.code = code;
        this.message = message;
    }
}
