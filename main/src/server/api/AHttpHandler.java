package server.api;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Created by Inventor
 */
public abstract class AHttpHandler implements HttpHandler {
    @Nullable
    protected Object doGet(@NotNull Map<String, String> parameterMap, @NotNull Map<EHeader, String> requestHeaderMap) throws NetworkException {
        throw new NetworkException(EError.UNSUPPORTED_METHOD, ERequestMethod.GET);
    }

    @Nullable
    protected Object doDelete(@NotNull Map<String, String> parameterMap, @NotNull Map<EHeader, String> requestHeaderMap) throws NetworkException {
        throw new NetworkException(EError.UNSUPPORTED_METHOD, ERequestMethod.DELETE);
    }

    @Nullable
    protected Object doPost(@NotNull Map<String, String> parameterMap, @NotNull Map<EHeader, String> requestHeaderMap) throws NetworkException {
        throw new NetworkException(EError.UNSUPPORTED_METHOD, ERequestMethod.POST);
    }

    @Nullable
    protected Object doPut(@NotNull Map<String, String> parameterMap, @NotNull Map<EHeader, String> requestHeaderMap) throws NetworkException {
        throw new NetworkException(EError.UNSUPPORTED_METHOD, ERequestMethod.PUT);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public final void handle(@NotNull HttpExchange httpExchange) {
        try {
            ERequestMethod requestMethod = ERequestMethod.getRequestMethod(httpExchange.getRequestMethod());
            if (requestMethod == null) throw new NetworkException(EError.UNSUPPORTED_METHOD, httpExchange.getRequestMethod());
            Map<EHeader, String> requestHeaderMap = EHeader.createHeaderMap(httpExchange.getRequestHeaders(), EHeaderType.REQUEST);
            if (requestHeaderMap.isEmpty()) throw new NetworkException(EError.REQUEST_HEADERS_NOT);
            Object responseObject = null;
            switch (requestMethod) {
                case GET: {
                    responseObject = doGet(createParameterMap(httpExchange), requestHeaderMap);
                    break;
                }
                case DELETE: {
                    responseObject = doDelete(createParameterMap(httpExchange), requestHeaderMap);
                    break;
                }
                case POST: {
                    responseObject = doPost(createParameterMap(httpExchange, requestHeaderMap), requestHeaderMap);
                    break;
                }
                case PUT: {
                    responseObject = doPut(createParameterMap(httpExchange, requestHeaderMap), requestHeaderMap);
                    break;
                }
            }
            sendJson(httpExchange, new JSimpleObject(responseObject));
        } catch (NetworkException e) {
            sendJson(httpExchange, new JSimpleMessage(e.getCode(), e.getMessage()));
        } catch (Exception e) {
            sendJson(httpExchange, new JSimpleMessage(EError.NETWORK_UNKNOWN_ERROR));
        }
    }

    @NotNull
    private static Map<String, String> createParameterMap(@NotNull HttpExchange httpExchange) {
        String query = httpExchange.getRequestURI().getQuery();
        if (query == null || query.isEmpty()) return Collections.emptyMap();
        else return createParameterMap(query);
    }

    @NotNull
    private static Map<String, String> createParameterMap(@NotNull HttpExchange httpExchange, @NotNull Map<EHeader, String> requestHeaderMap) throws NetworkException {
        String contentType = requestHeaderMap.get(EHeader.CONTENT_TYPE);
        if (contentType == null || contentType.isEmpty()) throw new NetworkException(EError.CONTENT_TYPE_NOT);
        if (contentType.startsWith("application/json")) {
            try {
                return createParameterMap(read(httpExchange.getRequestBody()));
            } catch (IOException e) {
                throw new NetworkException(e, EError.REQUEST_BODY_NOT_GET);
            }
        } else throw new NetworkException(EError.CONTENT_TYPE_NOT_CORRECT);
    }

    @NotNull
    private static Map<String, String> createParameterMap(@NotNull String content) {
        StringTokenizer st = new StringTokenizer(content, "&");
        Map<String, String> parameterMap = new HashMap<>(st.countTokens());
        while (st.hasMoreTokens()) {
            StringTokenizer st2 = new StringTokenizer(st.nextToken(), "=");
            if (st2.countTokens() != 2) continue;
            parameterMap.put(st2.nextToken(), st2.nextToken());
        }
        return parameterMap;
    }

    @NotNull
    private static String read(@NotNull InputStream is) throws IOException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {
            StringBuilder response = new StringBuilder();
            char[] buff = new char[1024];
            int read;
            while ((read = br.read(buff)) != -1) response.append(buff, 0, read);
            return response.toString();
        }
    }

    private static void sendJson(@NotNull HttpExchange httpExchange, @NotNull Object json) {
        String content = JsonHelper.toJson(json);
        byte[] bytes = content.getBytes(StandardCharsets.UTF_8);
        httpExchange.getResponseHeaders().add("Content-Type", "application/json;charset=utf-8");
        try {
            httpExchange.sendResponseHeaders(200, bytes.length);
            OutputStream os = httpExchange.getResponseBody();
            os.write(bytes);
            os.close();
        } catch (IOException ignore) {
            //TODO Логгирование
        }
    }
}
