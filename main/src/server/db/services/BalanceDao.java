package server.db.services;

import org.jetbrains.annotations.NotNull;
import server.db.entities.BalanceDB;

import java.math.BigDecimal;

/**
 * Created by Inventor
 */
interface BalanceDao extends BaseDao<BalanceDB> {
    boolean changeBalance(int accountId, @NotNull BigDecimal amount);
}
