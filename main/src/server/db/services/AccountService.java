package server.db.services;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import server.db.entities.AccountDB;

/**
 * Created by Inventor
 */
public final class AccountService extends BaseService<AccountDB> {
    public AccountService() {
        super(new AccountDaoImpl());
    }

    @Override
    @NotNull
    AccountDao getDao() {
        return (AccountDao) super.getDao();
    }

    @Nullable
    public AccountDB getAccountByLogin(@NotNull String login) {
        try {
            return getDao().getAccountByLogin(login);
        } catch (Exception ignore) {
            //TODO Логгирование
        }
        return null;
    }
}
