package server.db.services;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.jetbrains.annotations.NotNull;
import server.db.entities.AccountDB;
import server.db.entities.BalanceDB;
import server.db.entities.BalanceHistoryItemDB;
import server.db.entities.TokenDB;

/**
 * Created by Inventor
 */
final class HibernateSessionFactory {
    @NotNull
    private static SessionFactory sessionFactory;

    static {
        Configuration configuration = new Configuration().configure();
        configuration.addAnnotatedClass(AccountDB.class);
        configuration.addAnnotatedClass(BalanceDB.class);
        configuration.addAnnotatedClass(BalanceHistoryItemDB.class);
        configuration.addAnnotatedClass(TokenDB.class);
        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        sessionFactory = configuration.buildSessionFactory(builder.build());
    }

    @NotNull
    static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
