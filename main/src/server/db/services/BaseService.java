package server.db.services;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by Inventor
 */
public class BaseService<ObjectDB> {
    @NotNull
    private final BaseDao<ObjectDB> baseDao;

    BaseService(@NotNull BaseDao<ObjectDB> baseDao) {
        this.baseDao = baseDao;
    }

    @NotNull
    BaseDao<ObjectDB> getDao() {
        return baseDao;
    }

    @Nullable
    public final ObjectDB save(@NotNull ObjectDB objectDB) {
        try {
            baseDao.save(objectDB);
            return objectDB;
        } catch (Exception ignore) {
            //TODO Логгирование
        }
        return null;
    }

    @Nullable
    public final ObjectDB update(@NotNull ObjectDB objectDB) {
        try {
            baseDao.update(objectDB);
            return objectDB;
        } catch (Exception ignore) {
            //TODO Логгирование
        }
        return null;
    }

    @Nullable
    public final ObjectDB saveOrUpdate(@NotNull ObjectDB objectDB) {
        try {
            baseDao.saveOrUpdate(objectDB);
            return objectDB;
        } catch (Exception ignore) {
            //TODO Логгирование
        }
        return null;
    }

    public final boolean delete(int objectId) {
        try {
            return baseDao.delete(objectId);
        } catch (Exception ignore) {
            //TODO Логгирование
        }
        return false;
    }

    @Nullable
    public final ObjectDB get(int objectId) {
        try {
            return baseDao.get(objectId);
        } catch (Exception ignore) {
            //TODO Логгирование
        }
        return null;
    }

    public final void refresh(@NotNull ObjectDB objectDB) {
        try {
            baseDao.refresh(objectDB);
        } catch (Exception ignore) {
            //TODO Логгирование
        }
    }
}
