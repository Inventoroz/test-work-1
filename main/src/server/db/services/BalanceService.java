package server.db.services;

import org.jetbrains.annotations.NotNull;
import server.db.entities.BalanceDB;

import java.math.BigDecimal;

/**
 * Created by Inventor
 */
public final class BalanceService extends BaseService<BalanceDB> {
    public BalanceService() {
        super(new BalanceDaoImpl());
    }

    @Override
    @NotNull
    BalanceDao getDao() {
        return (BalanceDao) super.getDao();
    }

    public synchronized boolean changeBalance(int accountId, @NotNull BigDecimal amount) {
        try {
            return getDao().changeBalance(accountId, amount);
        } catch (Exception ignore) {
            //TODO Логгирование
        }
        return false;
    }
}
