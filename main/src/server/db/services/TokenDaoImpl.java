package server.db.services;

import org.hibernate.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import server.db.entities.TokenDB;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

/**
 * Created by Inventor
 */
final class TokenDaoImpl extends BaseDaoImpl<TokenDB> implements TokenDao {
    @NotNull
    @Override
    public Class<TokenDB> getObjectClass() {
        return TokenDB.class;
    }

    @Nullable
    @Override
    public TokenDB getTokenByToken(@NotNull String token) {
        try (Session session = HibernateSessionFactory.getSessionFactory().openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<TokenDB> criteria = builder.createQuery(TokenDB.class);
            Root<TokenDB> root = criteria.from(TokenDB.class);
            ParameterExpression<String> tokenParam = builder.parameter(String.class);
            criteria.select(root).where(builder.equal(root.get("token"), tokenParam));
            try {
                return session.createQuery(criteria)
                        .setParameter(tokenParam, token)
                        .getSingleResult();
            } catch (NoResultException ignored) {
                return null;
            }
        }
    }
}
