package server.db.services;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import server.db.entities.TokenDB;

/**
 * Created by Inventor
 */
interface TokenDao extends BaseDao<TokenDB> {
    @Nullable
    TokenDB getTokenByToken(@NotNull String token);
}
