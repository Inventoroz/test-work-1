package server.db.services;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.jetbrains.annotations.NotNull;
import server.db.entities.BalanceDB;
import server.db.entities.BalanceHistoryItemDB;

import java.math.BigDecimal;

/**
 * Created by Inventor
 */
final class BalanceDaoImpl extends BaseDaoImpl<BalanceDB> implements BalanceDao {
    @NotNull
    @Override
    public Class<BalanceDB> getObjectClass() {
        return BalanceDB.class;
    }

    @Override
    public boolean changeBalance(int accountId, @NotNull BigDecimal amount) {
        try (Session session = HibernateSessionFactory.getSessionFactory().openSession()) {
            BalanceDB balanceDB = session.find(BalanceDB.class, accountId);
            if (balanceDB == null) return false;
            BigDecimal balance = balanceDB.getBalance();
            BigDecimal newBalance = balance.add(amount);
            if (newBalance.compareTo(BigDecimal.ZERO) < 0) return false;
            Transaction transaction = session.beginTransaction();
            balanceDB.setBalance(newBalance);
            session.update(balanceDB);
            session.save(new BalanceHistoryItemDB(accountId, amount, System.currentTimeMillis()));
            transaction.commit();
        }
        return true;
    }
}
