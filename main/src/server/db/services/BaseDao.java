package server.db.services;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by Inventor
 */
interface BaseDao<ObjectDB> {
    @NotNull
    Class<ObjectDB> getObjectClass();

    void save(@NotNull ObjectDB objectDB);
    void update(@NotNull ObjectDB objectDB);
    void saveOrUpdate(@NotNull ObjectDB objectDB);
    boolean delete(int objectId);
    @Nullable
    ObjectDB get(int objectId);
    void refresh(@NotNull ObjectDB objectDB);
}
