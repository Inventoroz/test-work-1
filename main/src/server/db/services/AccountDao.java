package server.db.services;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import server.db.entities.AccountDB;

/**
 * Created by Inventor
 */
interface AccountDao extends BaseDao<AccountDB> {
    @Nullable
    AccountDB getAccountByLogin(@NotNull String login);
}
