package server.db.services;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import server.db.entities.TokenDB;

/**
 * Created by Inventor
 */
public final class TokenService extends BaseService<TokenDB> {
    public TokenService() {
        super(new TokenDaoImpl());
    }

    @Override
    @NotNull
    TokenDao getDao() {
        return (TokenDao) super.getDao();
    }

    @Nullable
    public TokenDB getTokenByToken(@NotNull String token) {
        try {
            return getDao().getTokenByToken(token);
        } catch (Exception ignore) {
            //TODO Логгирование
        }
        return null;
    }
}
