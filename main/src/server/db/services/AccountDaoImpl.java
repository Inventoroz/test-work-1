package server.db.services;

import org.hibernate.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import server.db.entities.AccountDB;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

/**
 * Created by Inventor
 */
final class AccountDaoImpl extends BaseDaoImpl<AccountDB> implements AccountDao {
    @NotNull
    @Override
    public Class<AccountDB> getObjectClass() {
        return AccountDB.class;
    }

    @Nullable
    @Override
    public AccountDB getAccountByLogin(@NotNull String login) {
        try (Session session = HibernateSessionFactory.getSessionFactory().openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<AccountDB> criteria = builder.createQuery(AccountDB.class);
            Root<AccountDB> root = criteria.from(AccountDB.class);
            ParameterExpression<String> loginParam = builder.parameter(String.class);
            criteria.select(root).where(builder.equal(root.get("login"), loginParam));
            try {
                return session.createQuery(criteria)
                        .setParameter(loginParam, login)
                        .getSingleResult();
            } catch (NoResultException ignored) {
                return null;
            }
        }
    }
}
