package server.db.services;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by Inventor
 */
abstract class BaseDaoImpl<ObjectDB> implements BaseDao<ObjectDB> {
    @Override
    public final void save(@NotNull ObjectDB objectDB) {
        try (Session session = HibernateSessionFactory.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(objectDB);
            transaction.commit();
        }
    }

    @Override
    public final void update(@NotNull ObjectDB objectDB) {
        try (Session session = HibernateSessionFactory.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(objectDB);
            transaction.commit();
        }
    }

    @Override
    public final void saveOrUpdate(@NotNull ObjectDB objectDB) {
        try (Session session = HibernateSessionFactory.getSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            session.saveOrUpdate(objectDB);
            transaction.commit();
        }
    }

    @Override
    public final boolean delete(int objectId) {
        try (Session session = HibernateSessionFactory.getSessionFactory().openSession()) {
            ObjectDB objectDB = session.find(getObjectClass(), objectId);
            if (objectDB == null) return false;
            Transaction transaction = session.beginTransaction();
            session.delete(objectDB);
            transaction.commit();
        }
        return true;
    }

    @Nullable
    @Override
    public final ObjectDB get(int objectId) {
        try (Session session = HibernateSessionFactory.getSessionFactory().openSession()) {
            return session.find(getObjectClass(), objectId);
        }
    }

    @Override
    public final void refresh(@NotNull ObjectDB objectDB) {
        try (Session session = HibernateSessionFactory.getSessionFactory().openSession()) {
            session.refresh(objectDB);
        }
    }
}
