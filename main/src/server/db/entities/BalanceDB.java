package server.db.entities;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * Created by Inventor
 */
@Entity
@Table(name = "balances")
public final class BalanceDB {
    @Id
    @Column(name = "account_id", unique = true, nullable = false)
    private int accountId;//Идентификатор аккаунта
    @SuppressWarnings("NullableProblems")
    @Column(name = "balance", nullable = false)
    @NotNull
    private BigDecimal balance;//Баланс

    public BalanceDB() {
    }

    public BalanceDB(int accountId, @NotNull BigDecimal balance) {
        this.accountId = accountId;
        this.balance = balance;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    @NotNull
    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(@NotNull BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BalanceDB that = (BalanceDB) o;
        return accountId == that.accountId;
    }

    @Override
    public int hashCode() {
        return accountId;
    }

    @NotNull
    @Override
    public String toString() {
        return "accountId=" + accountId + ", balance=" + balance;
    }
}
