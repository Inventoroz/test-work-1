package server.db.entities;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;

/**
 * Created by Inventor
 */
@Entity
@Table(name = "accounts")
public final class AccountDB {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "object_id", unique = true, nullable = false)
    private int objectId;//Идентификатор объекта базы данных
    @SuppressWarnings("NullableProblems")
    @Column(name = "login", unique = true, nullable = false)
    @NotNull
    private String login;//Логин
    @Column(name = "password")
    @Nullable
    private String password;//Пароль

    public AccountDB() {
    }

    public AccountDB(@NotNull String login, @Nullable String password) {
        this.login = login;
        this.password = password;
    }

    public int getObjectId() {
        return objectId;
    }

    public void setObjectId(int objectId) {
        this.objectId = objectId;
    }

    @NotNull
    public String getLogin() {
        return login;
    }

    public void setLogin(@NotNull String login) {
        this.login = login;
    }

    @Nullable
    public String getPassword() {
        return password;
    }

    public void setPassword(@Nullable String password) {
        this.password = password;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountDB that = (AccountDB) o;
        return objectId == that.objectId;
    }

    @Override
    public int hashCode() {
        return objectId;
    }

    @NotNull
    @Override
    public String toString() {
        return "objectId=" + objectId + ", login=" + login + ", password=" + password;
    }
}
