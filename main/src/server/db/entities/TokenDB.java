package server.db.entities;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;

/**
 * Created by Inventor
 */
@Entity
@Table(name = "tokens")
public final class TokenDB {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "object_id", unique = true, nullable = false)
    private int objectId;//Идентификатор объекта базы данных
    @Column(name = "account_id", nullable = false)
    private int accountId;//Идентификатор аккаунта
    @SuppressWarnings("NullableProblems")
    @Column(name = "token", nullable = false)
    @NotNull
    private String token;//Токен
    @Column(name = "created", nullable = false)
    private long created;//Дата создания
    @Column(name = "expiration", nullable = false)
    private long expiration;//Дата окончания действия

    public TokenDB() {
    }

    public TokenDB(int accountId, @NotNull String token, long created, long expiration) {
        this.accountId = accountId;
        this.token = token;
        this.created = created;
        this.expiration = expiration;
    }

    public int getObjectId() {
        return objectId;
    }

    public void setObjectId(int objectId) {
        this.objectId = objectId;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    @NotNull
    public String getToken() {
        return token;
    }

    public void setToken(@NotNull String token) {
        this.token = token;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getExpiration() {
        return expiration;
    }

    public void setExpiration(long expiration) {
        this.expiration = expiration;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TokenDB that = (TokenDB) o;
        return objectId == that.objectId;
    }

    @Override
    public int hashCode() {
        return objectId;
    }

    @NotNull
    @Override
    public String toString() {
        return "objectId=" + objectId + ", accountId=" + accountId + ", token=" + token + ", created=" + created + ", expiration=" + expiration;
    }
}
