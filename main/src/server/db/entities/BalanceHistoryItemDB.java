package server.db.entities;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by Inventor
 */
@Entity
@Table(name = "balance_history")
public final class BalanceHistoryItemDB {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "object_id", unique = true, nullable = false)
    private int objectId;//Идентификатор объекта базы данных
    @Column(name = "account_id", nullable = false)
    private int accountId;//Идентификатор аккаунта
    @SuppressWarnings("NullableProblems")
    @Column(name = "amount", nullable = false)
    @NotNull
    private BigDecimal amount;//Значение
    @Column(name = "created", nullable = false)
    private long created;//Дата создания

    public BalanceHistoryItemDB() {
    }

    public BalanceHistoryItemDB(int accountId, @NotNull BigDecimal amount, long created) {
        this.accountId = accountId;
        this.amount = amount;
        this.created = created;
    }

    public int getObjectId() {
        return objectId;
    }

    public void setObjectId(int objectId) {
        this.objectId = objectId;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    @NotNull
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(@NotNull BigDecimal amount) {
        this.amount = amount;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BalanceHistoryItemDB that = (BalanceHistoryItemDB) o;
        return objectId == that.objectId;
    }

    @Override
    public int hashCode() {
        return objectId;
    }

    @NotNull
    @Override
    public String toString() {
        return "objectId=" + objectId + ", accountId=" + accountId + ", amount=" + amount + ", created=" + created;
    }
}
