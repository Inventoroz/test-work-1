/*токены пользователей*/
DROP TABLE IF EXISTS `tokens`;
CREATE TABLE IF NOT EXISTS `tokens`
(
  `object_id` INT UNSIGNED NOT NULL AUTO_INCREMENT, /*идентификатор объекта базы данных*/
  `account_id` INT UNSIGNED NOT NULL DEFAULT 0, /*идентификатор аккаунта*/
  `token` VARCHAR(36) NOT NULL DEFAULT '', /*токен*/
  `created` BIGINT UNSIGNED NOT NULL DEFAULT 0, /*дата создания*/
  `expiration` BIGINT UNSIGNED NOT NULL DEFAULT 0, /*дата окончания действия*/
  PRIMARY KEY (`object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;