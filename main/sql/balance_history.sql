/*балансы пользователей*/
DROP TABLE IF EXISTS `balance_history`;
CREATE TABLE IF NOT EXISTS `balance_history`
(
  `object_id` INT UNSIGNED NOT NULL AUTO_INCREMENT, /*идентификатор объекта базы данных*/
  `account_id` INT UNSIGNED NOT NULL DEFAULT 0, /*идентификатор аккаунта*/
  `amount` DECIMAL(12,2) NOT NULL DEFAULT 0, /*значение*/
  `created` BIGINT UNSIGNED NOT NULL DEFAULT 0, /*дата создания*/
  PRIMARY KEY (`object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;