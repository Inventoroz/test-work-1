/*пользователи*/
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE IF NOT EXISTS `accounts`
(
  `object_id` INT UNSIGNED NOT NULL AUTO_INCREMENT, /*идентификатор объекта базы данных*/
  `login` VARCHAR(50) NOT NULL DEFAULT '', /*логин*/
  `password` VARCHAR(32) NULL DEFAULT NULL, /*пароль*/
  PRIMARY KEY (`object_id`),
  UNIQUE KEY `uqique_key_1` (`login`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `accounts` (`login`, `password`) VALUES ('test1', 'AE216BD9B0373DA5E1E261175C59D275');