/*балансы пользователей*/
DROP TABLE IF EXISTS `balances`;
CREATE TABLE IF NOT EXISTS `balances`
(
  `account_id` INT UNSIGNED NOT NULL DEFAULT 0, /*идентификатор аккаунта*/
  `balance` DECIMAL(12,2) UNSIGNED NOT NULL DEFAULT 0, /*баланс*/
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `balances` (`account_id`, `balance`) VALUES ('2', '8')